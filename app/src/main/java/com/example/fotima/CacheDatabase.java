package com.example.fotima;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sherzodbek on 01.05.2018 in Lesson5ICT4.
 */

public class CacheDatabase {
    private static CacheDatabase cacheDatabase;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private CacheDatabase(Context context) {
        preferences = context.getSharedPreferences("Topic_settings", Context.MODE_PRIVATE);
    }

    public static void init(Context context) {
        if (cacheDatabase == null) {
            cacheDatabase = new CacheDatabase(context);
        }
    }

    public static CacheDatabase getCacheDatabase() {
        return cacheDatabase;
    }
    ///////////////////////////////

    public String getFullName(){
        return preferences.getString("FULLNAME", "NAME");
    }
    public void setFullName(String fullName) {
        editor = preferences.edit();
        editor.putString("FULLNAME", fullName);
        editor.apply();
    }
    public String getRole(){
        return preferences.getString("ROLE", "none");
    }
    public void setRole(String role) {
        editor = preferences.edit();
        editor.putString("ROLE", role);
        editor.apply();
    }
}

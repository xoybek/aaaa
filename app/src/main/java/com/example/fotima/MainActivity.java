package com.example.fotima;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button button;
    private Button button2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button =  findViewById(R.id.reg_btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRegActivity();
            }
        });
        button2 = findViewById(R.id.enter_btn);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEnterActivity();
            }
        });
    }

        private void openRegActivity() {
        Intent intent = new Intent(this, RegistryActicity.class);
        startActivity(intent);
        }

        private void openEnterActivity()
        {
            Intent intent = new Intent(this, SelectLoginActivity.class);
            startActivity(intent);
        }
}

package com.example.fotima.models;

/**
 * Created by User on 09.08.2018.
 */

public class MessageData {
    private String userName;
    private String userNumber;
    private String userMessage;
    private String userTime;
    private int userType;

    public MessageData(String userName, String userNumber, String userMessage, String userTime, int userType) {
        this.userName = userName;
        this.userNumber = userNumber;
        this.userMessage = userMessage;
        this.userTime = userTime;
        this.userType = userType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public String getUserTime() {
        return userTime;
    }

    public void setUserTime(String userTime) {
        this.userTime = userTime;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }
}

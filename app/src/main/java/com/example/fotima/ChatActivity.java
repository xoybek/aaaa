package com.example.fotima;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fotima.adapters.MessageAdapter;
import com.example.fotima.models.MessageData;
import com.example.fotima.models.MessageDataBase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ChatActivity extends AppCompatActivity {
    private DatabaseReference databasePlaces;
    private ArrayList<MessageData> data = new ArrayList<>();
    private MessageAdapter adapter;
    private LinearLayoutManager manager;

    private RecyclerView list;
    private ImageView senderText;
    private EditText messageText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        list = findViewById(R.id.list);
        senderText = findViewById(R.id.senderText);
        messageText = findViewById(R.id.messageText);

        manager = new LinearLayoutManager(this);
        adapter = new MessageAdapter(data);
        databasePlaces = FirebaseDatabase.getInstance().getReference("messages");

        list.setLayoutManager(manager);
        list.setAdapter(adapter);

        senderText.setOnClickListener(view -> {
            String userMessage = messageText.getText().toString();
            if (!userMessage.equals("")) sendMessage(CacheDatabase.getCacheDatabase().getFullName(),
                    CacheDatabase.getCacheDatabase().getFullName()
                    , userMessage);
        });
    }

    private void sendMessage(String userName, String userNumber, String userMessage) {
        if (isOnline(this)) {
            Date currentTime = Calendar.getInstance().getTime();
            String id = databasePlaces.push().getKey();
            MessageDataBase messageDataBase = new
                    MessageDataBase(userName, userNumber, userMessage, currentTime.toString());
            databasePlaces.child(id).setValue(messageDataBase);
            messageText.setText("");
        } else {
            Toast.makeText(getApplicationContext(), "Internet bilan aloqani tekshiring", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isOnline(Context context) {
        if (context == null) return false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isOnline(this)) {
            databasePlaces.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String userNumber = CacheDatabase.getCacheDatabase().getFullName();
                    data.clear();
                    for (DataSnapshot messages : dataSnapshot.getChildren()) {
                        String username = messages.child("userName").getValue().toString();
                        String usernumber = messages.child("userNumber").getValue().toString();
                        String usermessage = messages.child("userMessage").getValue().toString();
                        String usertime = messages.child("userTime").getValue().toString();
                        MessageData messageData = new MessageData(username, usernumber, usermessage, usertime, userNumber.equals(usernumber) ? 1 : 2);
                        data.add(messageData);
                        adapter.notifyDataSetChanged();
                        manager.scrollToPosition(data.size() - 1);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Internet bilan aloqa yoq", Toast.LENGTH_LONG).show();
        }
    }
}
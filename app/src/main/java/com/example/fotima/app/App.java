package com.example.fotima.app;

import android.app.Application;

import com.example.fotima.CacheDatabase;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CacheDatabase.init(this);
    }
}

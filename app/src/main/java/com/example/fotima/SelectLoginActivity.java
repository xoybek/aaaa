package com.example.fotima;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class SelectLoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_login);
        findViewById(R.id.bemor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CacheDatabase.getCacheDatabase().setFullName("Bemor");
                startActivity(new Intent(SelectLoginActivity.this, MedAssistend.class));

            }
        });
        findViewById(R.id.shifokor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CacheDatabase.getCacheDatabase().setFullName("Shifokor");
                startActivity(new Intent(SelectLoginActivity.this, ShifokorMedAssistendActivity.class));
            }
        });
    }
}

package com.example.fotima;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.fotima.adapters.EnsiklopediyaAdapter;
import com.example.fotima.models.EnsiklopediyaData;

import java.util.ArrayList;

public class EnsiklopediyaActivity extends AppCompatActivity {
    private ArrayList<EnsiklopediyaData> data;
    private EnsiklopediyaAdapter adapter;
    private RecyclerView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ensiklopediya);
        list=findViewById(R.id.list);
        addData();
        adapter=new EnsiklopediyaAdapter(data);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);
    }

    private void addData() {
        data=new ArrayList<>();
        data.add(new EnsiklopediyaData("Allergologiya",R.drawable.allergologiya));
        data.add(new EnsiklopediyaData("Gastroentrologiya",R.drawable.gastroentrologiya));
        data.add(new EnsiklopediyaData("Ginekologiya",R.drawable.ginekologiya));
        data.add(new EnsiklopediyaData("Dermatologiya",R.drawable.dermotologiya));
            data.add(new EnsiklopediyaData("Infeksiyalar",R.drawable.infeksiyalar));
        data.add(new EnsiklopediyaData("Kardiologiya",R.drawable.kardiologiya));
        data.add(new EnsiklopediyaData("Dorilar qo`llanilishi",R.drawable.dorilar));
        data.add(new EnsiklopediyaData("Nevrologiya",R.drawable.nevrologiya));
        data.add(new EnsiklopediyaData("Oftalmologiya",R.drawable.oftalmologiya));
        data.add(new EnsiklopediyaData("Onkalogiya",R.drawable.onkologiya));
        data.add(new EnsiklopediyaData("Pulmonologiya",R.drawable.pulmonologiya));
        data.add(new EnsiklopediyaData("Revmotologiya",R.drawable.revmotologiya));
        data.add(new EnsiklopediyaData("Stomatologiya",R.drawable.stomotologiya));
        data.add(new EnsiklopediyaData("Terapiya",R.drawable.terapiya));
        data.add(new EnsiklopediyaData("To`g`ri ovqatlanish",R.drawable.ovqatlanish));
    }
}

package com.example.fotima.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fotima.R;
import com.example.fotima.models.MessageData;

import java.util.ArrayList;


/**
 * Created by User on 09.08.2018.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    private ArrayList<MessageData> data;

    public MessageAdapter(ArrayList<MessageData> data) {
        this.data = data;
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getUserType();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(viewType == 1 ?
                R.layout.chatitem_right : R.layout.chatitem_left, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nickName;
        private TextView message;
        private TextView time;
        private MessageData messageData;

        public ViewHolder(View itemView) {
            super(itemView);
            nickName = itemView.findViewById(R.id.nickName);
            message = itemView.findViewById(R.id.message);
            time = itemView.findViewById(R.id.time);
        }

        public void bindData(MessageData data) {
            messageData = data;
            nickName.setText(data.getUserName());
            message.setText(data.getUserMessage());
            time.setText(data.getUserTime());
        }
    }
}
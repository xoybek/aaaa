package com.example.fotima.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fotima.R;
import com.example.fotima.models.EnsiklopediyaData;

import java.util.ArrayList;

public class EnsiklopediyaAdapter extends RecyclerView.Adapter<EnsiklopediyaAdapter.EnsiklopediyaViewHolder> {
    private ArrayList<EnsiklopediyaData> data;

    public EnsiklopediyaAdapter(ArrayList<EnsiklopediyaData> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public EnsiklopediyaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EnsiklopediyaViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ensiklopediya, parent, false
        ));
    }

    @Override
    public void onBindViewHolder(@NonNull EnsiklopediyaViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class EnsiklopediyaViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView image;

        public EnsiklopediyaViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
        }

        void bind(EnsiklopediyaData data) {
            image.setImageResource(data.getImage());
            name.setText(data.getName());
        }
    }
}
